
RUN [ "touch", "/tmp/1" ]
RUN [ "apt", "-y", "update" ]
RUN [ "apt", "-y", "dist-upgrade" ]
RUN [ "apt", "-y", "install", "openssh-server", "curl", "iputils-ping", "vim", "zip", "unzip" ]
RUN [ "mkdir", "/run/sshd", "/root/.ssh" ]
COPY --chown=root:root debian/id_ed25519.pub /root/.ssh/authorized_keys
RUN [ "chmod", "go-rwx", "-Rc", "/root/.ssh" ]
ENTRYPOINT [ "/usr/sbin/sshd", "-D" ]

apt -y install openssh-server
mkdir -pv /run/sshd /root/.ssh
cp -vfa /tmp/inst/debian/id_ed25519.pub /root/.ssh/authorized_keys
chmod go-rwx -Rc /root/.ssh
