#!/bin/bash -x
./builder.sh \
  --src "dedoc:$1" \
  --dev \
  --mount ./"$1" /dedoc \
  --mount ./blob/apt-archives /var/cache/apt/archives \
  --mount ./blob/apt-lists /var/lib/apt/lists \
