#!/bin/bash
echo "We generate the dedoc distro (docker images) on an empty docker install. We need a working bash (cygwin or linux) for that."

./builder.sh \
  --preserve-tmp \
  --src debian:bullseye \
  --installer /dedoc/installer.sh \
  --mount ./core /dedoc \
  --mount ./blob/apt-archives /var/cache/apt/archives \
  --mount ./blob/apt-lists /var/lib/apt/lists \
  --dst dedoc:core
