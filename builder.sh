#!/bin/bash
export src=""
export dst=""
export dev=n
export installer=""
declare -A mounts
export mounts
export preserve_tmp=n

shopt -s lastpipe

do_fatal() {
  echo "$0: $*" >&2
  exit 1
}

do_help() {
  echo "$0:
--help: prints this
--src: sets source image (as in \"docker images\"). No default, required.
--dst: sets target image (as in \"docker images\"). No default. Required if no \"--dev\" flag was given.
--installer: sets the installer script (executable). Must be a path inside of the container. Can be anything. Its last output must be its container id (\$HOSTNAME). No default. Required, if no \"--dev\" flag was given.
--dev: gives a shell in a newly created container. No dst and installer are used or required.
--mount <host-path> <container-path>: mounts a path, only for the build
--mount-file <host-path>: mounts pathes from a host config. It must be a text file in host-path<space>container-path\\n form. No other spaces allowed.
--preserve-tmp: preserves temp file for later analyse"
  exit 0
}

abs_path() {
  if [ "${1:0:1}" = "/" ]
  then
    echo "$1"
  elif [ "$OSTYPE" = "cygwin" ]
  then
    cygpath -a -w -l "$1"
  else
    realpath "$1"
  fi
}

do_src_img() {
  export src="$1"
}

do_dst_img() {
  export dst="$1"
}

do_installer_sh() {
  export installer="$1"
}

do_add_mount() {
  mounts["$1"]="$2"
}

do_add_mount_file() {
  while read l
  do
    do_add_mount "${l% *}" "${l#* }"
  done <"$1"
}

while [ "$#" -gt 0 ]
do
  if [ "$1" = "--help" ]
  then
    do_help
    exit 0
  elif [ "$1" = "--src" ]
  then
    shift
    do_src_img "$1"
    shift
  elif [ "$1" = "--dst" ]
  then
    shift
    do_dst_img "$1"
    shift
  elif [ "$1" = "--installer" ]
  then
    shift
    do_installer_sh "$1"
    shift
  elif [ "$1" = "--dev" ]
  then
    shift
    dev=y
  elif [ "$1" = "--mount" ]
  then
    shift
    do_add_mount "$1" "$2"
    shift
    shift
  elif [ "$1" = "--mount-file" ]
  then
    shift
    do_add_mount_file "$1"
    shift
  elif [ "$1" = "--preserve-tmp" ]
  then
    shift
    export preserve_tmp="y"
  else
    do_fatal "unknown argument $1, use --help"
  fi
done

if [ "$src" = "" ]
then
  do_fatal "no source image, use --help"
fi

if [ "$dst" = "" ] && [ "$dev" = n ]
then
  do_fatal "no destination image, use --help"
fi

if [ "$installer" = "" ] && [ "$dev" = n ]
then
  do_fatal "no installer, use --help"
fi

set -e
set -x

mountflags=""
for i in "${!mounts[@]}"
do
  mountflags="${mountflags} --mount src=$(abs_path ${i}),target=${mounts[$i]},type=bind"
done
mountflags="${mountflags# }"

tmp_dir="$(mktemp -d)"
[ "$preserve_tmp" = "y" ] && echo "temp dir: $tmp_dir"

docker image inspect "$src" >/dev/null 2>&1 || docker pull "$src"

if [ "$dev" = "y" ]
then
  docker run -ti $mountflags "$src" /bin/bash
else
  docker run -t $mountflags "$src" /bin/bash -c "$installer;"' echo "DEDOC_CONTAINER_ID=${HOSTNAME}"' 2>&1|tee "$tmp_dir/out"
  container_id="$(grep ^DEDOC_CONTAINER_ID= <"$tmp_dir/out"|tail -1|sed 's/^DEDOC_CONTAINER_ID=//g')"
  if [ "$container_id" = "" ]
  then
    do_fatal "Installer error"
  fi
  container_id="${container_id//[^a-z0-9]/}"
  echo "found container id $container_id, committing"
  docker commit "$container_id" "$dst"
fi

[ "$preserve_tmp" = n ] && rm -rvf "$tmp_dir"
